#include <cstdio>
#include <cstdlib>
#include <climits>
#include <iostream>

using namespace std;

struct Edge
{
    int src;
    int dest;
    int weight;
};

struct Graph
{
    int V;
    int E;
    struct Edge* edge;
};

struct Graph* createGraph(int V, int E)
{
    struct Graph* graph = (struct Graph*) malloc(sizeof(struct Graph));
    graph->V = V;
    graph->E = E;
    graph->edge = (struct Edge*) malloc(graph->E * sizeof(struct Edge));
    return graph;
}

void BellmanFord(struct Graph* graph, int source)
{
    int V = graph->V;
    int E = graph->E;
    int dist[V];
    int howMany = 0;
    
    for (int i = 0; i < V; i++)
        dist[i] = INT_MAX;
    dist[source] = 0;
    
    for (int i = 1; i <= V-1; i++)
    {
        for (int j = 0,howMany = 0; j < E; j++)
        {
            int u = graph->edge[j].src;
            int v = graph->edge[j].dest;
            int weight = graph->edge[j].weight;
            if (dist[u] != INT_MAX && dist[u] + weight < dist[v])
                dist[v] = dist[u] + weight;
            howMany++;
        }
        if(howMany == 0)
            break;
    }
    
    for(int i = 1; i <=V-1;i++){
        for (int j = 0; j < E; j++)
        {
            int u = graph->edge[j].src;
            int v = graph->edge[j].dest;
            int weight = graph->edge[j].weight;
            if ((dist[u] != INT_MAX && dist[u] + weight < dist[v]) || dist[u]==INT_MIN){
                dist[v]=INT_MIN;
                howMany++;
            }
        }
        if(howMany == 0)
            break;
    }
    
    for(int i=0 ; i< V ;i++){
        if(dist[i]==INT_MAX)
            printf("U\n");
        if(dist[i]==INT_MIN)
            printf("I\n");
        if(dist[i]!=INT_MAX && dist[i]!=INT_MIN)
            printf("%d\n",dist[i]);
    }
    return;
}

int main(int argc, const char * argv[]) {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    int init_vertices,init_edges,init_starter,start_vertex = -1,end_vertex,weight;
    scanf("%d %d",&init_vertices,&init_edges);
    scanf("%d",&init_starter);
    
    struct Graph *graph = createGraph(init_vertices, init_edges);
    
    for(int i = 0; i < init_edges; i++){
        scanf("%d %d %d",&start_vertex,&end_vertex,&weight);
        graph->edge[i].src = start_vertex-1;
        graph->edge[i].dest = end_vertex-1;
        graph->edge[i].weight = weight;
    }
    
    BellmanFord(graph,init_starter-1);
    return 0;
}
