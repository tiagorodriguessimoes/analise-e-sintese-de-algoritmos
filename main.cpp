#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <stdio.h>
using namespace std;

struct node {
    int info;
    node *next;
};

class Queue {
private:
    node *front;
    node *rear;
public:
    Queue();
    ~Queue();
    bool isEmpty();
    void enqueue(int);
    int dequeue();
    void display();
    
};

void Queue::display(){
    node *p = new node;
    p = front;
    if(front == NULL){
        cout<<"\nNothing to Display\n";
    }else{
        while(p!=NULL){
            cout<<endl<<p->info;
            p = p->next;
        }
    }
}

Queue::Queue() {
    front = NULL;
    rear = NULL;
}

Queue::~Queue() {
    delete front;
}

void Queue::enqueue(int data) {
    node *temp = new node();
    temp->info = data;
    temp->next = NULL;
    if(front == NULL){
        front = temp;
    }else{
        rear->next = temp;
    }
    rear = temp;
}

int Queue::dequeue() {
    node *temp = new node();
    int value=0;
    if(front == NULL){
        cout<<"\nQueue is Emtpty\n";
    }else{
        temp = front;
        value = temp->info;
        front = front->next;
        delete temp;
    }
    return value;
}

bool Queue::isEmpty() {
    return (front == NULL);
}


class Graph {
private:
    int n;
    set<int>* A;
public:
    Graph(int size = 2);
    ~Graph();
    bool isConnected(int, int);
    void addEdge(int u, int v);
    std::set<int> adjacentes(int u);
    void BFS(int );
};

Graph::Graph(int size) {
    
    if (size < 2) n = 2;
    else n = size;
    
    A = new set<int>[n];
}

Graph::~Graph() {
    
}

bool Graph::isConnected(int u, int v) {
    
    return (A[u-1].find(v-1) == A[u-1].end());
}

void Graph::addEdge(int u, int v) {
    
    A[u-1].insert(v-1);
    A[v-1].insert(u-1);
}

std::set<int> Graph::adjacentes(int u) {
    return A[u-1];
}


void Graph::BFS(int s) {
    
    Queue Q;
    vector<bool> explored(n+1);
    vector<int> distanceToRoot(n+1);
    vector<int> distanceCounter(n+1);
    int max=-1;
    
    for (int i = 1; i <= n; ++i){
        explored[i] = false;
        distanceToRoot[i] = 0;
        distanceCounter[i] = 0;
    }
    
    Q.enqueue(s);
    explored[s] = true;
    
    while (!Q.isEmpty()) {
        int v = Q.dequeue();
        std::set<int> adj = adjacentes(v);
        for (std::set<int>::iterator it = adj.begin(); it != adj.end(); ++it) {
            int w = (*it)+1;
            if (!explored[w]) {/** adds the vertex w to the queue */
                Q.enqueue(w);/** marks the vertex w as visited */
                explored[w] = true;
                distanceToRoot[w] = distanceToRoot[v] + 1;
                distanceCounter[distanceToRoot[v] + 1] = distanceCounter[distanceToRoot[v] + 1] + 1;
                if (distanceToRoot[w] > max)
                    max = distanceToRoot[w];
            }
        }
    }
    printf("%d\n", max);
    for (int i = 0; i < max; ++i){
        printf("%d\n", distanceCounter[i+1]);
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    int init_vertices,init_edges,init_starter,u,v;
    
    scanf("%d %d",&init_vertices,&init_edges);
    scanf("%d",&init_starter);
    Graph g(init_vertices);                     // creates a graph with NUM vertices
    for(int i = 0; i < init_edges; i++){
        scanf("%d %d",&u,&v);
        g.addEdge(u, v);                        // add edges to the graph
    }
    g.BFS(init_starter);
    return 0;
}
